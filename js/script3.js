var mesureForm = document.getElementById("mesure_form");
var solutionsContainer = document.getElementById("solutions");
var form = document.getElementById("form");
var mesure = 0;
var sp = [45,60,90];
var sc = [15,30];
var m = 30;
var all = sc.concat(sp);


//Create an input and send value after each modification

var newInput = document.createElement("input");
newInput.setAttribute("value", mesure);
newInput.id = "kitchenLength";
newInput.className = "input_length"
mesureForm.appendChild(newInput);

var ktchLength = document.getElementById("kitchenLength");
ktchLength.onchange = function(){
    this.setAttribute("value",this.value);
    mesure=this.value;
}


//Functions

computeElt = function(serie,d){
let solutions = new Array; 
    serie.forEach(function(elt){
        let rest=d%elt; 
        let nbElt=Math.trunc(d/elt);
        let allElt = [rest];
        for(i=0; nbElt>i; i++){
            allElt.push(elt);
        };
        solutions.push(allElt);
    }); 
return solutions;
}

computeRest = function(solutionsIn){
    let solutionsOut = new Array;
    solutionsIn.forEach(function(elt){
        if(elt[0]>=Math.min(...all)){
            let rest=computeElt(all,elt[0]);
            for(i=0;i<rest.length;i++){
                if(rest[i][0]!=elt[0]){
                    let x=rest[i].concat(elt);
                    x.splice(rest[i].length,1,);
                    solutionsOut.push(x);
                }
            }
        }
        else{
            solutionsOut.push(elt);
        }
    });
    return solutionsOut;
}

uniqueArray = function(u){
    u.forEach(function(e){e.sort(function(a, b) {return a - b;})});
    var j={};
    u.forEach(function(v){
        j[v+'::' + typeof v] = v;
    })

    return Object.keys(j).map(function(v){
        return j[v];
    })
}

createInputButton = function(name){
    let newButton = document.createElement("button")
    newButton.id = "send_"+name;
    newButton.textContent = name;
    mesureForm.appendChild(newButton);
    }

createFormButton = function(){
    let newButton = document.createElement("button")
    newButton.id = "submitEquipement";
    newButton.textContent = "submit";
    form.appendChild(newButton);
}

createContainer = function(containerId,containerWidth){
    let newContainer = document.createElement("div");
    newContainer.id = containerId;
    newContainer.className = "container";
    newContainer.style.width = containerWidth+"px";
    solutionsContainer.appendChild(newContainer);
}

createContainerElt = function(containerId,eltId,eltWidth){
    let newContainerElt = document.createElement("div");
    newContainerElt.id = eltId;
    newContainerElt.className = "elt";
    newContainerElt.style.width = eltWidth-2+"px";
    newContainerElt.textContent = eltWidth;
    document.getElementById(containerId).appendChild(newContainerElt);
}

generateAppSolutions = function(solutions){
    for(i=0; i<solutions.length; i++){
            createContainer("cont-"+[i],mesure);
        for(j=1;j<solutions[i].length;j++){
            createContainerElt("cont-"+[i],"cont-"+[i]+"-elt-"+[j],solutions[i][j]);
        }
    }
}

showNbSolution = function(nbSolution,nbExactSolution){
    document.getElementById("nbSolution").textContent = nbSolution;
    document.getElementById("showSolution").textContent = "voir "+nbExactSolution;
}

showExactSolution = function(exactSolutions){
    solutionsContainer.innerHTML = "";
    generateAppSolutions(exactSolutions);
}

function intersection(arrays) {
    array = [];
    for (var i = 0; i < arguments.length; i++)
      array.push(arguments[i]);
  
    var result = [];
  
    for(var i = 0; i < array.length - 1; i++) {
      for(var j = 0; j < array[i].length; j++) {
        if (array[i+1].includes(array[i][j]))
          result.push(array[i][j]);
      }
    }
  
    return result;
  }

//Compute the different solutions

createInputButton("Compute");

document.getElementById("send_Compute").onclick = function(){

    solutionsContainer.innerHTML = "";

    var computeOne = function(d){
        let r = d.splice(0,1);
        let a = new Array;
        for(i=0; i<sp.length; i++){
            let x=[r-sp[i],sp[i]];
            if(x[0]>=0){
                a.push(x);
            }
            else{
                for(l=0;l<sc.length;l++){
                        y=[r-sc[l],sc[l]];
                    if(y[0]>=0 && y[0]<m){
                        a.push(y);
                    }
                    else{
                        if(r<Math.min(...sc)){
                            a.push(r)
                            break
                        }
                    }
                }
            break
            }
        }
        for(j=0; j<a.length; j++){
            let a0=a[j].concat(d);
            a.splice(j,1,a0);
        }
        return a;
    };
    
    var computeAll = function(o){
        n=new Boolean(0);
        do{ 
            let g=new Array;
            o.forEach(element => {
                let cO= computeOne(element);
                g=g.concat(cO);
                return g;       
            });
            for(i=0;i<g.length;i++){
                if(g[i][0]>=Math.min(...sc)){
                    n=Boolean(0);
                    break
                }
                else{
                    n=Boolean(1);
                }
            };
            o=g
        }
        while(n!=Boolean(1));
        o=uniqueArray(o);
        return o;
    }

affichage = computeAll([[mesure]]);
affichage.sort(function(a, b){
    return a.length - b.length;
  });
generateAppSolutions(affichage);

let exactSolution = affichage.filter(exact => exact[0]==0);
showNbSolution(affichage.length,exactSolution.length);
document.getElementById("showSolution").onclick = function(){
    showExactSolution(exactSolution);
};

}







